# Revolutionary album administrator - RAA

After checkout, follow these steps, to start your local raa.
1. `npm install`
2. `npm serve`
3. Go to `localhost:8080` and administrate your albums.

The raa is based on [vue3](https://v3.vuejs.org/), [vuex](https://vuex.vuejs.org/) and [vue-router](https://router.vuejs.org/).

## Concept
See [solution](loesung1.md) for more information about the concept of this small app.

## Instances
[Stage-instance](https://best-album-administrator-stage.herokuapp.com)

[Prod-instance](https://best-album-administrator-prod.herokuapp.com)

[Prod-instance on own server](https://hktest1.christianratz.de)

## List albums
![listed albums](documentation/list_albums.png)

## Add album
![album form](documentation/add_album.png)

## Update album
![update album](documentation/update_album.png)

## Delete album
![delete album](documentation/delete_album.png)

