# Hk Test | Aufgabe 1 - Lösung

Für die Umsetzung dieser kleinen SPA habe ich mich für Vuejs als stützendes Framework entschieden.
Ich habe bereits Applikationen mit Angular, React und Meteor umgesetzt. Da ich noch nie mit Vuejs in Berührung gekommen bin und ich immer interessiert an etwas Neuem bin, hat sich das nun angeboten.

Meine Applikation (siehe ```aufgabe1```) besteht aus:

- Komponenten (```components```): Die entsprechenden Views für die Anzeige und das Bearbeiten von Alben
- Store (```store```): Einem ```vuex```-gestützten Store, für das Erstellen, Abfragen, Aktualisieren und Löschen von Alben
- Router (```router.js```): Einem Router, der je nach Route eine entsprechende View bereitstellt

## (Small) problem
If multiple users access the application at the same time and update, create or delete albums, their albumlist and album-information could be different on each client unless they will reload the list of albums or reload their app.
