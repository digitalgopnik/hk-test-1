import { createApp } from 'vue';

import router from './router.js';
import store from './store/index.js';
import App from './App.vue';
import Entry from './components/ui/Entry.vue';
import Btn from './components/ui/Btn.vue';
import Badge from './components/ui/Badge.vue';
import Spinner from './components/ui/Spinner.vue';
import Dialog from './components/ui/Dialog.vue';

const app = createApp(App);

app.use(router);
app.use(store);

app.component('raa-entry', Entry);
app.component('raa-btn', Btn);
app.component('raa-badge', Badge);
app.component('raa-spinner', Spinner);
app.component('raa-dialog', Dialog);

app.mount('#app');
