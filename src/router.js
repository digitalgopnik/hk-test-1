import { createRouter, createWebHistory } from 'vue-router';

import AlbumList from './pages/albums/AlbumList.vue';
import AlbumDetail from './pages/albums/AlbumDetail.vue';
import AddAlbum from './pages/albums/AddAlbum.vue';
import NotFound from './pages/NotFound.vue';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    // list of albums
    { path: '/', component: AlbumList },
    // detail-view for album
    {
      path: '/albums/:id',
      component: AlbumDetail,
      props: true,
    },
    // view to add an album
    { path: '/add', component: AddAlbum },
    // not found page
    { path: '/:notFound(.*)', component: NotFound }
  ]
});

export default router;
