import { createStore } from 'vuex';

import albumsModule from './modules/albums/index.js';

/**
 * initialize store
 */
const store = createStore({
  modules: {
    albums: albumsModule,
  },
  state() {},
  getters: {}
});

export default store;