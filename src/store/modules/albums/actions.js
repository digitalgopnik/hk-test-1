import axios from 'axios';

/**
 * 
 */
export default {

  /**
   * delete and album
   * 
   * @param {*} context 
   * @param {*} data 
   */
  async deleteAlbum(context, id) {
    const response = await axios(
      {
        url: `https://hk-test-api.herokuapp.com/albums/${id}`, 
        method: 'DELETE',
        headers: { 
          'X-API-Key': 'secret', 
        },
      }
    );

    if (!response.status === 204) {
      const error = new Error(response.message || 'Failed to fetch!');
      throw error;
    }

    context.commit('deleteAlbum', id);
    context.commit('setFetchTimestamp');
  },

  /**
   * add an album
   * 
   * @param {*} context 
   * @param {*} data 
   */
  async addAlbum(context, data) {
    const albumData = {
      title: data.title,
      artist: data.artist,
      released_in: +(data.released_in),
    };

    const response = await axios(
      {
        url: 'https://hk-test-api.herokuapp.com/albums', 
        method: 'POST',
        data: JSON.stringify(albumData),
        headers: { 
          'X-API-Key': 'secret', 
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
      }
    );

    // const responseData = await response.json();
    if (!response.data) {
      const error = new Error(response.data.message || 'Failed to fetch!');
      throw error;
    }

    context.commit('addAlbum', {
      ...response.data
    });
    context.commit('setFetchTimestamp');
  },

  /**
   * update an existing album
   * 
   * @param {*} context 
   * @param {*} data 
   */
  async updateAlbum(context, data) {
    const albumData = {
      title: data.title,
      artist: data.artist,
      released_in: data.released_in,
    };

    const response = await axios(
      {
        url: `https://hk-test-api.herokuapp.com/albums/${data.id}`,
        data: JSON.stringify(albumData),
        method: 'PUT',
        headers: { 
          'X-API-Key': 'secret', 
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
      }
    );

    if (!response.data) {
      const error = new Error(response.data.message || 'Failed to fetch!');
      throw error;
    }

    context.commit('updateAlbum', {
      ...response.data
    });
    context.commit('setFetchTimestamp');
  },

  /**
   * request api to get all albums and load them in state
   * 
   * @param {*} context 
   * @param {*} payload 
   * @returns 
   */
  async loadAlbums(context, payload) {
    if (!payload.forceRefresh && !context.getters.shouldUpdate) {
      return;
    }

    const response = await axios(
      {
        url: 'https://hk-test-api.herokuapp.com/albums',
        method: 'GET',
        headers: { 'X-API-Key': 'secret', 'Accept': 'application/json' }
      }
    );

    if (!response.data) {
      const error = new Error(response.data.message || 'Failed to fetch!');
      throw error;
    }

    context.commit('setAlbums', response.data);
    context.commit('setFetchTimestamp');
  },

  /**
   * request api to get all albums and load them in state
   * 
   * @param {*} context 
   * @param {*} albumId 
   * @returns 
   */
   async loadAlbum(context, albumId) {

    const response = await axios(
      {
        url: `https://hk-test-api.herokuapp.com/albums/${albumId}`,
        method: 'GET',
        headers: { 'X-API-Key': 'secret', 'Accept': 'application/json' }
      }
    );

    if (!response.data) {
      const error = new Error(response.data.message || 'Failed to fetch!');
      throw error;
    }

    context.commit('setAlbum', response.data);
  }
};
