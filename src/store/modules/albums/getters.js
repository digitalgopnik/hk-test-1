export default {
  albums(state) {
    return state.albums;
  },
  album(state) {
    return state.album;
  },
  hasAlbums(state) {
    return state.albums && state.albums.length > 0;
  },
  shouldUpdate(state) {
    const lastFetch = state.lastFetch;
    if (!lastFetch) {
      return true;
    }
    const currentTimeStamp = new Date().getTime();
    return (currentTimeStamp - lastFetch) / 1000 > 60;
  }
};