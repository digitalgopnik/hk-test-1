export default {
  /**
   * deletes an album from state
   * 
   * @param {*} state app-state
   * @param {*} id    id of album
   */
  deleteAlbum(state, id) {
    state.albums = state.albums.filter(album => +(album.id) !== +(id));
  },
  /**
   * updates an album in state
   * 
   * @param {*} state   app-state
   * @param {*} payload payload with updated-album-data
   */
   updateAlbum(state, payload) {
    state.albums = state.albums.filter(album => +(album.id) !== +(payload.id));
    state.albums.push(payload);
  },
  /**
   * adds an albums to state
   * 
   * @param {*} state   app-state
   * @param {*} payload payload with created-album-data
   */
  addAlbum(state, payload) {
    state.albums.push(payload);
  },
  /**
   * set albums in state
   * 
   * @param {*} state   app-state
   * @param {*} payload payload with albums
   */
  setAlbums(state, payload) {
    state.albums = payload;
  },
  /**
   * set album in state
   * 
   * @param {*} state   app-state
   * @param {*} payload payload with album
   */
   setAlbum(state, payload) {
    state.album = payload;
  },
  /**
   * set timestamp when last fetched
   * 
   * @param {*} state app-state
   */
  setFetchTimestamp(state) {
    state.lastFetch = new Date().getTime();
  }
};